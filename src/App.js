import React from "react";

import ReactToExcel from "react-html-table-to-excel";
import { Thead, Tbody } from "./components/";
import Pdf from "react-to-pdf";
import jsPDF from "jspdf";
import "jspdf-autotable";

const ref = React.createRef();

const App = () => {
  const heads = ["Firstname", "Lastname", "Phone", "Email"];
  const values = [
    ["Hamid", "Mohamadi", "09120658719", "a@gmail.com"],
    ["Sara", "Eslami", "09120658719", "b@gmail.com"],
    ["Bahar", "Bahari", "09120658719", "c@gmail.com"],
  ];

  const exportPDF = () => {
    const doc = new jsPDF();
    doc.autoTable({ html: "#table-to-xls" });
    doc.save("table.pdf");
  };

  return (
    <div className="App">
      <h2 className="text-center text-capitalize">
        export to excel and PDF in react
      </h2>
      <table className="table table-hover" id="table-to-xls" ref={ref}>
        <Thead heads={heads} />
        <Tbody data={values} />
      </table>

      <ReactToExcel
        className="btn btn-primary"
        table="table-to-xls"
        filename="excelFile"
        sheet="sheet 1"
        buttonText="EXPORT TO EXCEL"
      />
      {/* <Pdf targetRef={ref} filename="exportPDF.pdf">
        {({ toPdf }) => (
          <button onClick={toPdf} className="btn btn-danger ml-2">
            EXPORT TO PDF
          </button>
        )}
      </Pdf> */}

      <button className="btn btn-danger ml-2" onClick={exportPDF}>
        EXPORT TO PDF
      </button>
    </div>
  );
};

export default App;
