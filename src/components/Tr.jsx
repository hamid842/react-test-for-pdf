import React from "react";

export default ({ heads }) => {
  return (
    <tr>
      {heads.map((td) => {
        return <td key={td}>{td}</td>;
      })}
    </tr>
  );
};
