import React from "react";
import Tr from "./Tr";
export default ({ data }) => {
  return (
    <tbody>
      {data.map((value) => {
        return <Tr heads={value} key={value} />;
      })}
    </tbody>
  );
};
