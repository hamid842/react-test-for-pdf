import React from "react";

export default ({ heads }) => {
  return (
    <thead>
      <tr>
        {heads.map((th) => {
          return <th key={`th${th}`}>{th}</th>;
        })}
      </tr>
    </thead>
  );
};
